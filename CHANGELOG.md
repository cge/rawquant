# 0.3.0

-   Fix bug in cycles for tcprotocol.
-   Move plate\_setup handling to qslib.
-   Change build system to setup.cfg and setuptools\_scm.\
-   Moved TC to a new file, in potential prep for moving to qslib.
-   Fixed cycle off-by-one in TC: now matches reality!
-   Made tcplot actually useful for multi-temperature experiments!
-   New all\_points dataframe for TC
-   Removed etree storage: maybe can be pickled now?

# 0.2.0

-   Add qslib-based in-progress run loading.
-   Change experiment time calculation: use RunStartTime in EDS file (if
    available), rather than using the first read time in the message
    log. This will change data, but is the better choice, especially for
    runs where reading start late in the protocol.

# 0.1.0

-   Fixed bug that showed incorrect change in temperature/time per cycle
    when auto-delta was not enabled for a stage.

-   Added display of last-cycle temperature in protocol printout, for
    multi-cycle steps with a temperature delta per cycle.

-   Changed tests to Python 3.8, instead of a mix of 3.7 and latest.

# 0.0.1

-   Initial release.
