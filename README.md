[![coverage report](https://framagit.org/cge/rawquant/badges/master/coverage.svg)](https://framagit.org/cge/rawquant/commits/master)
[![pipeline report](https://framagit.org/cge/rawquant/badges/master/pipeline.svg)](https://framagit.org/cge/rawquant/commits/master)
[![Documentation Status](https://readthedocs.org/projects/rawquant/badge/?version=latest)](https://rawquant.readthedocs.io/en/latest/?badge=latest)
[![Pylint](https://framagit.org/cge/rawquant/-/jobs/artifacts/master/raw/pylint/pylint.svg?job=pylint)](https://framagit.org/cge/rawquant/-/jobs/artifacts/master/raw/pylint/pylint.log?job=pylint)

NOTE: RawQuant is now largely incorporated into
[QSLib](https://github.com/cgevans/qslib/). It should be more reliable
and useful, and new improvements will be added there. If you need
same-function compatibility, however, rawquant still works.

RawQuant (in need of a better name) is a small library for reading (and eventually manipulating) the experiment files for Applied Biosystems' QuantStudio qPCR machines.  It is intended to give lower-level control and data access than the QuantStudio software.

At the moment, it can give a nice summary of information about the experiment, and reasonably raw data from finished experiment files.  for example:

```python
import rawquant
import matplotlib.pyplot as plt

experiment = rawquant.Experiment("a_quantstudio_experiment.eds")

# A summary of the experiment
print(experiment)

# A dataframe of all the planned step times and information per the protocol:
experiment.tcprotocol.dataframe()

# A fluorescence vs temperature plot for well E5 excitation filter 1 with emission filter 1:
plt.plot(experiment.rawdata.loc["x1-m1"]["temperature_avg"],
	     experiment.rawdata.loc["x1-m1"]["E5"])

# A florescence vs time since experiment start plot for the same:
# Note that the time here is a timedelta64[ns], so you will most
# likely want to do something to make the display make sense.
plt.plot(experiment.rawdata.loc["x1-m1"]["exptime"],
	     experiment.rawdata.loc["x1-m1"]["E5"])
```
