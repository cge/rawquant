"""
Module for reading and handling QuantStudio experiment files.

The main useful class at the moment is Experiment, which allows the loading
and processing of the files.
"""
from __future__ import annotations
import re
import asyncio
import concurrent.futures
import ast
import zipfile
from typing import BinaryIO, List, Optional, Tuple, Iterable, Union, cast
import lxml.etree as etree
from qslib.qsconnection_async import QSConnectionAsync
from qslib.data import df_from_readings
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt  # pylint: disable=import-outside-toplevel
from qslib.plate_setup import PlateSetup
from .tcprotocol import TCProtocol, TCStage, TCStep, _durformat  # noqa


_WELLNAMES = [x + str(y) for x in "ABCDEFGH" for y in range(1, 13)]

PointRef = Tuple[str, int, int, int, int]


def _parse_fd_fn(x: str) -> PointRef:
    s = re.search(r"S(\d{2})_C(\d{3})_T(\d{2})_P(\d{4})_M(\d)_X(\d)_filterdata.xml$", x)
    if s is None:
        raise ValueError("Could not parse 'x'")
    return (f"x{s[6]}-m{s[5]}", int(s[1]), int(s[2]), int(s[3]), int(s[4]))


def _index_to_filename_ref(i: PointRef) -> str:
    x, s, c, t, p = i
    return f"S{s:02}_C{c:03}_T{t:02}_P{p:04}_M{x[4]}_X{x[1]}"


def _filters_from_tcxml(tcxml: etree.Element) -> List[str]:  # type: ignore
    cps = tcxml.findall("CollectionProfile")
    if len(cps) > 1:
        raise ValueError("Too many collection profiles")

    fs = [x.find("FilterSet") for x in cps[0].findall("CollectionCondition")]
    fss = [x.attrib["Excitation"] + "-" + x.attrib["Emission"] for x in fs]
    return fss


def _oxfordlist(iterable: Iterable[str]) -> str:
    x = iter(iterable)
    s = next(x)  # we know the first will be there
    try:
        maybeult = next(x)
    except StopIteration:
        return s
    try:
        nextult = next(x)
        s = s + ", " + maybeult
        maybeult = nextult
    except StopIteration:
        return s + " and " + maybeult
    while True:
        try:
            nextult = next(x)
            s = s + ", " + maybeult
            maybeult = nextult
        except StopIteration:
            return s + ", and " + maybeult


class Experiment:
    """
    A QuantStudio eds/edt experiment file.

    str / print gives a nice summary.

    Attributes
    ----------
    rawdata
        The raw fluorescence data, combined with timing and temperature data
        from the run.  Is `None` for experiments without data (eg, files that
        have not yet been run).

    filtersets
        A list of filtersets enabled for the experiment, in 'xI-mJ' format.

    experiment_name
        The experiment name.

    experiment_state
        The state of the experiment.  Currently known values are 'INIT' and 'COMPLETE'.

    """

    def __init__(self, file_or_path: Union[str, BinaryIO]):
        """
        Open, and parse the data, from a completed QuantStudio experiment
        file (.eds).

        Parameters
        ----------

        file_or_path: file-like or str
            Either the path to an EDS file, or a readable, open file
            descriptor for the file.  This is passed to
            `zipfile.ZipFile`.

        """
        zipbase: Optional[zipfile.ZipFile] = zipfile.ZipFile(file_or_path)

        try:
            _tcprotocolxml = etree.parse(zipbase.open("apldbio/sds/tcprotocol.xml"))
        except KeyError:
            _tcprotocolxml = None

        try:
            _experimentxml = etree.parse(zipbase.open("apldbio/sds/experiment.xml"))

            # rsts = _experimentxml.find("RunStartTime")  # type: ignore
            # if rsts is not None:
            #    self.start_time: Optional[float] = float(rsts.text) / 1000
            # else:
            #    self.start_time = None
        except KeyError:
            _experimentxml = None
            # self.start_time = None

        try:
            _filterdataxml = etree.parse(zipbase.open("apldbio/sds/filterdata.xml"))
            _msglog: Optional[bytes] = zipbase.open("apldbio/sds/messages.log").read()
            self.start_time = float(
                re.search(rb"^Run ([\d.]+) Stage 1$", _msglog, re.MULTILINE)[1]
            )
            self._rawdata = _parse_filterdata(
                _filterdataxml, _msglog, self.start_time  # type: ignore
            )
        except KeyError:
            self._rawdata = None
            self._filterdataxml = None
            self.start_time = None

        try:
            _platesetupxml = etree.parse(zipbase.open("apldbio/sds/plate_setup.xml"))
        except KeyError:
            _platesetupxml = None

        self.fdc: Optional[pd.DataFrame] = None

        self.__init_attribs__(_tcprotocolxml, _platesetupxml, _experimentxml)

    def __init_attribs__(self, _tcprotocolxml, _platesetupxml, _experimentxml) -> None:
        if _tcprotocolxml is not None:
            self.tcprotocol = TCProtocol.from_etree(_tcprotocolxml)
            self.filtersets = _filters_from_tcxml(_tcprotocolxml)
        if _platesetupxml is not None:
            self.plate_setup = PlateSetup.from_platesetup_xml(_platesetupxml)
            self.sample_table = self.plate_setup.to_table

        if _experimentxml is not None:
            self.experiment_name = _experimentxml.find("Name").text
            self.experiment_state = _experimentxml.find("RunState").text

    @property
    def sample_wells(self):
        return self.plate_setup.sample_wells

    @sample_wells.setter
    def sample_wells(self, nv):
        self.plate_setup.sample_wells = nv

    @property
    def well_sample(self):
        return self.plate_setup.well_sample

    @property
    def samples(self):
        self.plate_setup.samples_by_name

    @classmethod
    async def from_running_machine_async(
        cls, password: str, hostname: str, port: int
    ) -> Experiment:
        # pylint: disable=protected-access
        c = QSConnectionAsync(hostname, port, password=password)
        exp = cast(Experiment, cls.__new__(cls))

        async with c:
            experiment_name = await c.get_run_title()
            _platesetupxml = etree.fromstring(  # type: ignore
                await c.get_sds_file("plate_setup.xml", runtitle=experiment_name)
            )

            _experimentxml = etree.fromstring(  # type: ignore
                await c.get_sds_file("experiment.xml", runtitle=experiment_name)
            )

            exp.start_time = float(await c.get_run_start_time())

            _tcprotocolxml = etree.fromstring(  # type: ignore
                await c.get_sds_file("tcprotocol.xml", runtitle=experiment_name)
            )

            exp.fdc = await c.get_all_filterdata(experiment_name)

        exp.fdc["exptime"] = exp.fdc["time"] - exp.start_time
        exp._rawdata = _fdc_to_rawdata(exp.fdc, exp.start_time)

        exp.__init_attribs__(_tcprotocolxml, _platesetupxml, _experimentxml)

        return exp

    @classmethod
    def from_running_machine(
        cls, password: str, hostname: str = "localhost", port: int = 7000
    ) -> Experiment:
        pool = concurrent.futures.ThreadPoolExecutor()
        res = cast(
            Experiment,
            pool.submit(
                asyncio.run, cls.from_running_machine_async(password, hostname, port)
            ).result(),
        )
        return res

    @property
    def rawdata(self) -> pd.DataFrame:
        """The fluorescence data from the file.  The rows are indexed by
        (filterset, stage, cycle, step, point) (point is mostly
        superfluous, being equivalent to step, but for melt curve stages).
        The columns are standard well references, with the addition of
        some other columns: time (the exact datetime of the data
        collection), exptime (a datetime duration from the start of the
        experiment), temperature_avg (the average temperature across the
        six heat block reagions), and temperature_X for X = 0 to 5 (the
        temperature for each region).

        Filtersets are rows rather than columns because the times are
        slightly different.  You can easily use `.loc(filterset,:)` to
        narrow the data to a particular filterset.
        """
        if self._rawdata is None:
            raise ValueError("Experiment does not have data")
        return self._rawdata

    @property
    def fldata(self) -> pd.DataFrame:
        """An alias for rawdata"""
        if self._rawdata is None:
            raise ValueError("Experiment does not have data")
        return self._rawdata

    def __str__(self) -> str:
        s = f"QS Experiment {self.experiment_name} ({self.experiment_state}):\n\n"
        s += str(self.tcprotocol)
        s += "\n\n"
        s += f"Filter sets: {_oxfordlist(self.filtersets)}\n"
        s += "\n"
        s += str(self.plate_setup)
        return s

    def tcplot(
        self, ax: Optional[plt.Axes] = None
    ) -> Tuple[plt.Axes, Tuple[List[plt.Line2D], List[plt.Line2D]]]:
        """Return a plot"""
        return self.tcprotocol.tcplot(ax=ax)


def _redo_wells(wn: str) -> str:
    "change fluorescent well name format"
    return f"{wn[2]}{int(wn[3:])}"


def _fdc_to_rawdata(fdc: pd.DataFrame, start_time: float) -> pd.DataFrame:
    ret: pd.DataFrame = fdc.loc[:, slice("f_A01", "f_H12")].copy()
    ret.rename(columns=_redo_wells, inplace=True)
    for i in range(0, 6):
        ret[f"temperature_{i+1}"] = fdc[f"tr_A{(i+1)*2:02}"]
    ret["temperature_avg"] = ret.loc[:, slice("temperature_1", "temperature_6")].mean(
        axis=1
    )
    ret["time"] = fdc["time"]
    ret["exptime"] = fdc["exptime"]
    ret["exphrs"] = (ret["time"] - start_time) / 60 / 60
    # ret['time'] = ret['time'].astype('datetime64[s]') # fixme: should we?

    return ret


def _parse_filterdata(
    xmldata: etree.ElementTree, msglog: bytes, start_time: Optional[float]  # type: ignore
) -> pd.DataFrame:
    c = re.compile(rb"Debug (\d+\.\d+) Start XML:IMAGe\+ \S+\.tiff ({[^}]+})\n")
    alllog = c.findall(msglog)
    mdatd = [ast.literal_eval(x[1].decode()) for x in alllog]
    mdat = (
        pd.DataFrame(mdatd)
        .astype(
            {
                "exposureIndex": int,
                "exposure": int,
                "time": "float",
                "lampTemperature": float,
                "point": int,
                "step": int,
                "stage": int,
                "cycle": int,
            }
        )
        .astype({"time": "float"})
    )
    mdat.loc[:, "exem"] = (  # type: ignore
        mdat.loc[:, "excitationColor"] + "-" + mdat.loc[:, "emissionColor"]
    )
    mdat = mdat.loc[lambda x: x["exposure"] > 100, :]  # type: ignore
    if start_time is not None:
        mdat.loc[:, "exptime"] = mdat.loc[:, "time"] - start_time
    else:
        mdat.loc[:, "exptime"] = mdat.loc[:, "time"] - mdat.loc[0, "time"]

    mdat = mdat.set_index(
        ["exem", "stage", "cycle", "point", "step"], verify_integrity=True
    )

    def _getpdattr(x: etree.Element, n: str, t: type = int) -> np.ndarray:  # type: ignore
        return np.array(
            [
                t(y)
                for y in x.xpath(
                    f"//PlateData/Attribute/key[text()='{n}']/../value/text()"
                )
            ]
        )

    fattribs = pd.DataFrame.from_dict(
        {
            "exem": _getpdattr(xmldata, "FILTER_SET", str),  # type: ignore
            "stage": _getpdattr(xmldata, "STAGE", int),  # type: ignore
            "step": _getpdattr(xmldata, "STEP", int),  # type: ignore
            "cycle": _getpdattr(xmldata, "CYCLE", int),  # type: ignore
            "point": _getpdattr(xmldata, "POINT", int),  # type: ignore
        }
    )

    fwelldat = pd.DataFrame(
        [
            np.fromstring(x, sep="\t")
            for x in xmldata.xpath("//PlateData/WellData/text()")
        ],
        columns=_WELLNAMES,
    )

    alltemperaturecols = [f"temperature_{i}" for i in range(1, 7)]
    ftempdat = pd.DataFrame(
        [
            np.fromstring(x, sep=",")
            for x in xmldata.xpath(
                "//PlateData/Attribute/key[text()='TEMPERATURE']/../value/text()"
            )
        ],
        columns=alltemperaturecols,
    )

    ftempdat.loc[:, "temperature_avg"] = np.average(ftempdat, axis=1)  # type: ignore

    fdat = fattribs.join(fwelldat).join(ftempdat)
    fdat = fdat.set_index(["exem", "stage", "cycle", "point", "step"])

    ret = fdat.join(mdat.loc[:, ["time", "exptime"]]).sort_index()
    ret["exphrs"] = (ret["time"] - start_time) / 60 / 60  # type: ignore

    return ret


QSDataFile = Experiment
