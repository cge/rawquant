from __future__ import annotations

import textwrap
from typing import Iterable, List, Optional, Tuple, Union, cast
import lxml.etree as etree
from dataclasses import dataclass
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

ALLTEMPS = ["temperature_{}".format(i) for i in range(1, 7)]

BOOL_INTERP = {"false": False, "true": True}


def _temperature_str(temperatures: Union[float, Iterable[float]]) -> str:
    """Tries to do the right thing in generating a string for a temperature or
    list of temperatures."""
    if isinstance(temperatures, Iterable):
        temperatures = list(temperatures)
        if len(temperatures) == 1 or len(set(temperatures)) == 1:
            return f"{temperatures[0]:.02f} °C"
        else:
            return "[" + ", ".join(f"{x:.02f}" for x in temperatures) + "]" + " °C"
    else:
        return f"{temperatures:.02f} °C"


@dataclass
class TCStep:
    """A thermal protocol step"""

    hold_time: float
    temperatures: List[float]
    collect_data: bool = False
    ext_temperature: float = 0.0
    ext_time: float = 0
    ramp_rate: float = 1.6

    @classmethod
    def from_etree(cls, stepelem: etree.Element) -> TCStep:  # type: ignore
        """Create a TCStep from an XML element."""
        temperatures = [float(x) for x in stepelem.xpath("Temperature/text()")]
        collect_data = bool(int(stepelem.find("CollectionFlag").text))
        hold_time = float(stepelem.find("HoldTime").text)
        ext_temperature = float(stepelem.find("ExtTemperature").text)
        ext_time = float(stepelem.find("ExtHoldTime").text)
        ramp_rate = float(stepelem.find("RampRate").text)
        return cls(
            hold_time, temperatures, collect_data, ext_temperature, ext_time, ramp_rate
        )

    def duration_at_cycle(
        self, cycle: int, ext_enabled: bool = False
    ) -> float:  # cycle from 1
        "Duration of the step (excluding ramp) at `cycle` (from 1)"
        return self.hold_time + (cycle) * self.ext_time * int(ext_enabled)
        # FIXME: is this right?

    def temperatures_at_cycle(
        self, cycle: int, ext_enabled: bool = False
    ) -> List[float]:
        "Temperatures of the step at `cycle` (from 1)"
        return [
            x + (cycle) * self.ext_temperature * float(ext_enabled)
            # FIXME: This actually applies to first cycle... why!?
            for x in self.temperatures
        ]

    def info_str(self, cycles: int = 1, ext_enabled: bool = False) -> str:
        "String describing the step."

        tempstr = _temperature_str(self.temperatures)
        if (cycles > 1) and ext_enabled and (self.ext_temperature != 0.0):
            t = _temperature_str(self.temperatures_at_cycle(cycles, ext_enabled))
            tempstr += f" to {t}"

        elems = [f"{tempstr} for {_durformat(self.hold_time)}/cycle"]
        if ext_enabled:
            if self.ext_temperature != 0:
                elems.append(f"{self.ext_temperature} °C/cycle")
            if self.ext_time != 0:
                elems.append(f"{_durformat(self.ext_time)}/cycle")
        if self.ramp_rate != 1.6:
            elems.append(f"{self.ramp_rate} °C/s ramp")
        s = ", ".join(elems)

        if self.collect_data:
            s += " (collects data)"

        return s

    def __str__(self) -> str:
        return self.info_str(0, True)


@dataclass
class TCStage:
    """A thermal protocol stage"""

    steps: List[TCStep]
    cycles: int = 1
    delta_enabled: bool = False

    @classmethod
    def from_etree(cls, stageelem: etree.Element) -> TCStage:  # type:ignore
        "Create TCStage from XML element."
        cycles = int(stageelem.find("NumOfRepetitions").text)
        delta_enabled = BOOL_INTERP[stageelem.find("AutoDeltaEnabled").text]
        steps = [TCStep.from_etree(x) for x in stageelem.findall("TCStep")]
        flag = stageelem.find("StageFlag").text
        if flag not in ["CYCLING", "PRE_CYCLING", "INFINITE_HOLD"]:
            raise ValueError(f"{flag} not supported")
        return cls(steps, cycles, delta_enabled)

    def dataframe(
        self, start_time: float = 0, previous_temperatures: Optional[List[float]] = None
    ) -> pd.DataFrame:
        """
        Create a dataframe of the steps in this stage.

        Parameters
        ----------

        start_time
            The initial start time, in seconds, of the stage (before
            the ramp to the first step).  Default is 0.

        previous_temperatures
            A list of temperatures at the end of the previous stage, to allow
            calculation of ramp time.  If `None`, the ramp is assumed to take
            no time.
        """

        durations = np.array(
            [
                step.duration_at_cycle(i, self.delta_enabled)
                for i in range(1, self.cycles + 1)
                for step in self.steps
            ]
        )
        temperatures = np.array(
            [
                step.temperatures_at_cycle(i, self.delta_enabled)
                for i in range(1, self.cycles + 1)
                for step in self.steps
            ]
        )
        ramp_rates = np.array(
            [step.ramp_rate for _ in range(1, self.cycles + 1) for step in self.steps]
        )
        collect_data = np.array(
            [
                step.collect_data
                for _ in range(1, self.cycles + 1)
                for step in self.steps
            ]
        )

        # FIXME: is this how ramp rates actually work?

        ramp_durations = np.zeros(len(durations))
        if previous_temperatures is not None:
            ramp_durations[0] = (
                np.max(np.abs(temperatures[0] - previous_temperatures)) / ramp_rates[0]
            )

        ramp_durations[1:] = (
            np.max(np.abs(temperatures[1:] - temperatures[:-1]), axis=1)
            / ramp_rates[1:]
        )

        tot_durations = durations + ramp_durations

        start_times = start_time + np.zeros(len(durations))
        start_times[0] = start_time + ramp_durations[0]
        start_times[1:] = (
            start_time + np.cumsum(tot_durations[:-1]) + ramp_durations[1:]
        )

        end_times = start_time + np.cumsum(tot_durations)

        data = pd.DataFrame(
            {
                "start_time": start_times,
                "end_time": end_times,
                "collect_data": collect_data,
            }
        )

        data["temperature_avg"] = np.average(temperatures, axis=1)

        for i in range(
            0, temperatures.shape[1]
        ):  # pylint: disable=unsubscriptable-object
            data["temperature_{}".format(i + 1)] = temperatures[:, i]

        data["cycle"] = [
            c for c in range(1, self.cycles + 1) for s in range(1, len(self.steps) + 1)
        ]
        data["step"] = [
            s for c in range(1, self.cycles + 1) for s in range(1, len(self.steps) + 1)
        ]

        data.set_index(["cycle", "step"], inplace=True)

        return data

    def __str__(self) -> str:
        if self.cycles > 1:
            adds = "s"
        else:
            adds = ""
        stagestr = f"{self.cycles} cycle{adds}"
        if len(self.steps) > 1:
            stepstrs = [
                f" - Step {i+1}: {step.info_str(self.cycles, self.delta_enabled)}"
                for i, step in enumerate(self.steps)
            ]
            stagestr += " of\n" + "\n".join(stepstrs)
        else:
            stagestr += " at " + self.steps[0].info_str(self.cycles, self.delta_enabled)
        return stagestr


def _durformat(time: Union[int, float]) -> str:
    """Convert time in seconds to a nice string"""
    s = ""
    # <= 2 minutes: stay as seconds
    if time <= 2 * 60:
        s = "{}s".format(int(time)) + s
        return s

    if (time % 60) != 0:
        s = "{}s".format(int(time % 60)) + s
    rtime = int(time // 60)
    # <= 2 hours: stay as minutes
    if time <= 2 * 60 * 60:
        s = "{}m".format(rtime) + s
        return s

    if (rtime % 60) != 0:
        s = "{}m".format(rtime % 60) + s
    rtime = rtime // 60
    # <= 3 days: stay as hours
    if time <= 3 * 24 * 3600:
        if (rtime % 24) != 0:
            s = "{}h".format(rtime) + s
        return s

    s = "{}h".format(rtime % 24) + s
    rtime = rtime // 24
    # days without bound
    s = "{}d".format(rtime) + s
    return s


@dataclass
class TCProtocol:
    """A thermal protocol."""

    stages: List[TCStage]

    @classmethod
    def from_etree(cls, protelem: etree.Element) -> TCProtocol:  # type: ignore
        "Create TCProtocol from XML element."
        stages = [TCStage.from_etree(x) for x in protelem.findall("TCStage")]
        return cls(stages)

    @property
    def dataframe(self) -> pd.DataFrame:
        "A DataFrame of the temperature protocol."
        dataframes: List[pd.DataFrame] = []

        stage_start_time = 0.0
        stagenum = 1
        previous_temperatures = None

        for stage in self.stages:
            dataframe = stage.dataframe(stage_start_time, previous_temperatures)
            stagenum += 1
            previous_temperatures = dataframe.iloc[-1].loc[ALLTEMPS].to_numpy()
            stage_start_time = cast(float, dataframe["end_time"].iloc[-1])
            dataframes.append(dataframe)

        return pd.concat(
            dataframes,
            keys=range(1, len(dataframes) + 1),
            names=["stage", "step", "cycle"],
        )

    @property
    def all_points(self) -> pd.DataFrame:
        d = self.dataframe
        dd = (
            d.loc[:, "temperature_avg":]
            .iloc[np.repeat(np.arange(0, len(d)), 2)]
            .reset_index()
        )
        times = np.empty(len(dd))
        times[::2] = np.array(d.loc[:, "start_time"])
        times[1::2] = np.array(d.loc[:, "end_time"])
        dd.insert(0, "time", times)
        return dd

    @property
    def all_times(self) -> np.ndarray:
        "An array of all start and end times of each step, interleaved."
        d = self.dataframe
        alltimes = np.empty(len(d) * 2)
        alltimes[:-1:2] = d["start_time"]
        alltimes[1::2] = d["end_time"]
        return alltimes

    @property
    def all_temperatures(self) -> np.ndarray:
        "An array of temperature settings at `all_times`."
        d = self.dataframe
        return np.repeat(d["temperature_avg":], 2)

    def tcplot(
        self, ax: Optional[plt.Axes] = None
    ) -> Tuple[plt.Axes, Tuple[List[plt.Line2D], List[plt.Line2D]]]:
        "A plot of the temperature and data collection points."
        #    try:

        #    except ModuleNotFoundError:
        #        raise "
        if ax is None:
            _, ax = plt.subplots()

        d = self.dataframe

        all_points = self.all_points

        # FIXME: 0 to 6 is specific
        p1 = [
            ax.plot(
                all_points["time"] / 3600.0,
                all_points[f"temperature_{i}"],
                label=f"{i}",
            )[0]
            for i in range(1, 7)
        ]

        dc = d.loc[d.collect_data, :]
        p2 = [
            ax.plot(
                cast("pd.Series['float']", dc["end_time"]) / 3600.0,
                dc[f"temperature_{i+1}"],
                ".",
                color=p.get_color(),
            )[0]
            for i, p in enumerate(p1)
        ]

        ax.legend(title="zone")
        ax.set_xlabel("experiment time (hrs) (points collect data)")
        ax.set_ylabel("temperature (°C)")
        ax.set_title("Temperature Protocol")

        return (ax, (p1, p2))

    def __str__(self) -> str:
        begin = (
            "Temperature Protocol (total time "
            f"{_durformat(cast(float, self.dataframe['end_time'].iloc[-1]))})):\n"
        )
        stagestrs = [
            " - " + textwrap.indent(f"Stage {i+1}: " + str(stage), "  ")[2:]
            for i, stage in enumerate(self.stages)
        ]

        return begin + "\n".join(stagestrs)
