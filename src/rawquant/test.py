import unittest
import os.path
from rawquant import Experiment, TCStep, _durformat


def gettestfile(filename: str) -> str:
    return os.path.join(os.path.dirname(__file__), "../testdata/", filename)


class ExperimentTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.experiment = Experiment(gettestfile("testdata.eds"))
        cls.unfinished = Experiment(gettestfile("unfinished.edt"))

    def test_unsupported_melt_curve(self):
        self.assertRaises(ValueError, Experiment, gettestfile("unsupported.edt"))

    def test_file_not_found(self):
        self.assertRaises(FileNotFoundError, Experiment, gettestfile("nonexistent.edt"))

    def test_tcprotocol_total_time(self):
        total_time = self.experiment.tcprotocol.dataframe.iloc[-1].end_time
        self.assertAlmostEqual(total_time, 410_415.625)

    def test_tcprotocol_all_times(self):
        all_times = self.experiment.tcprotocol.all_times
        self.assertEqual(len(all_times), 488)
        self.assertEqual(all_times[0], 0)
        self.assertAlmostEqual(all_times[-1], 410_415.625)
        self.assertAlmostEqual(all_times[-150], 275_415.25)

    def test_filtersets(self):
        self.assertEqual(self.experiment.filtersets, ["x1-m1", "x1-m4", "x4-m4"])

    def test_tcplot(self):
        # We just hope the plot works
        self.experiment.tcplot()

    def test_experiment(self):
        self.assertEqual(self.experiment.experiment_state, "COMPLETE")
        self.assertEqual(self.unfinished.experiment_state, "INIT")

    def test_tcprotocol(self):
        tcprotocol = self.experiment.tcprotocol
        self.assertEqual(len(tcprotocol.stages), 9)
        self.assertEqual(len(tcprotocol.stages[2].steps), 2)

    def test_tcstage(self):
        teststage = self.experiment.tcprotocol.stages[2]
        self.assertEqual(len(teststage.steps), 2)
        self.assertAlmostEqual(teststage.cycles, 10)

    def test_tcstep(self):
        teststep = self.experiment.tcprotocol.stages[2].steps[1]
        self.assertEqual(
            teststep,
            TCStep(30 * 60, [52.0, 52.0, 52.0, 52.0, 52.0, 52.0], True, -0.1, 0.0, 1.6),
        )

    def test_fldata(self):
        self.assertIs(self.experiment.fldata, self.experiment.rawdata)

    def test_print_works(self):
        str(self.experiment)
        str(self.unfinished)

    def test_durformat(self):
        self.assertEqual("3h", _durformat(3600 * 3))
        self.assertEqual("120m", _durformat(3600 * 2))
        self.assertEqual("3d3h1m3s", _durformat(24 * 3600 * 3 + 3600 * 3 + 63))
